import React from 'react'

export default function MyComponent() {
  return (
    <div>
      <h2>This is sample component to work on!</h2>
    </div>
  );
}
